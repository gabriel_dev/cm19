<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <title>CM-19</title>
    <!-- BOOTSTRAP -->
    <link rel="stylesheet" href="css/bootstrap/bootstrap.min.css">
    <script src="js/lib/jquery/jquery-3.5.1.min.js"></script>
    <script src="js/lib/popper/popper.min.js"></script>
    <script src="js/lib/bootstrap/bootstrap.min.js"></script>
    <!-- FONT AWESOME -->
    <script src="https://kit.fontawesome.com/95452840ac.js" crossorigin="anonymous"></script>
    <!-- CHARTJS -->
    <script src="https://cdn.jsdelivr.net/npm/chart.js@2.9.3/dist/Chart.min.js"></script>
    <!-- CUSTOM CSS -->
    <link rel="stylesheet" href="css/main.css">

</head>

<body>

    <div class="d-flex" id="wrapper">
        <!-- Sidebar -->
        <div class="bg-light border-right" id="sidebar-wrapper">
            <div class="sidebar-heading">Covid Manager 19</div>
            <div class="list-group list-group-flush">
                <a class="list-group-item list-group-item-action bg-light pointer-btn" data-toggle="modal"
                    data-target="#myModal">Registrar Nuevo Paciente</a>
                <a class="list-group-item list-group-item-action bg-light pointer-btn" data-toggle="modal"
                    data-target="#updateModal">Actualizar Paciente</a>
                <a class="list-group-item list-group-item-action bg-light pointer-btn" data-toggle="modal" data-target="#searchModal">Ver Pacientes Registrados</a>
            </div>
        </div>
        <!-- SideBar -->

        <!-- Page Content -->
        <div id="page-content-wrapper">
            <nav class="navbar navbar-expand-lg navbar-light bg-light border-bottom">
                <!-- Open SideBar -->
                <button class="btn btn-primary" id="menu-toggle"><i class="fas fa-bars"></i></button>
                <!-- Open SideBar -->

                <!-- Menu -->
                <div class="collapse navbar-collapse" id="navbarSupportedContent">
                    <ul class="navbar-nav ml-auto mt-2 mt-lg-0">
                        <li class="nav-item">
                            <a class="nav-link" href="#">Details</a>
                        </li>
                        <li>
                            <a class="nav-link" href="#">Students</a>
                        </li>
                    </ul>
                </div>
                <!-- Menu -->
            </nav>

            <!-- Main Content -->
            <div class="container-fluid vh-100 pl-4 pr-4 bg-body">
                <div class="row h-100">
                    <div class="col-md-4 pt-4 pb-5">
                        <div class="row h-100">
                            <div class="col-md-12 h-25">
                                <div class="card h-100 sc-card">
                                    <div class="card-header bg-success text-white">
                                        <i class="fas fa-heartbeat"></i> Curados
                                    </div>
                                    <div class="card-body">
                                        <canvas id="myChart" height="100%"></canvas>
                                        <script>
                                            var ctx = document.getElementById("myChart").getContext("2d");
                                            var mychart = new Chart(ctx, {
                                                type: "line",
                                                data: {
                                                    labels: ['January', 'February', 'March', 'April', 'May'],
                                                    datasets: [{
                                                        label: 'My First dataset',
                                                        borderColor: 'rgb(0, 148, 50)',
                                                        data: [0, 10, 5, 35, 8],
                                                        backgroundColor: 'rgba(0, 148, 50, 0.4)'
                                                    }]
                                                }
                                            });
                                        </script>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-12 h-75">
                                <div class="card h-100 mt-4 sc-card">
                                    <div class="card-header bg-primary text-white">
                                        <i class="fas fa-chart-bar"></i> General
                                    </div>
                                    <div class="card-body">
                                        <canvas id="myChart2" height="200" class="c-chart"></canvas>
                                        <script>
                                            var ctx = document.getElementById("myChart2").getContext("2d");
                                            var mychart = new Chart(ctx, {
                                                type: "doughnut",
                                                data: {
                                                    labels: ['January', 'February', 'March'],
                                                    datasets: [{
                                                        label: 'My First dataset',
                                                        borderColor: [
                                                            'rgb(238, 90, 36)',
                                                            'rgb(6, 82, 221)',
                                                            'rgb(27, 20, 100)'
                                                        ],
                                                        data: [20, 10, 5],
                                                        backgroundColor: [
                                                            'rgb(238, 90, 36)',
                                                            'rgb(6, 82, 221)',
                                                            'rgb(27, 20, 100)'
                                                        ]
                                                    }]
                                                }
                                            });
                                        </script>
                                        <table class="table c-table text-center">
                                            <thead>
                                                <tr class="bg-primary text-white">
                                                    <th scope="col">Ciudad</th>
                                                    <th scope="col">Infectados</th>
                                                    <th scope="col">Curados</th>
                                                    <th scope="col">Muertos</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Bogotá</td>
                                                    <td>Mark</td>
                                                    <td>Otto</td>
                                                    <td>@mdo</td>
                                                </tr>
                                                <tr>
                                                    <td>Medellín</td>
                                                    <td>Jacob</td>
                                                    <td>Thornton</td>
                                                    <td>@fat</td>
                                                </tr>
                                                <tr>
                                                    <td>Huila</td>
                                                    <td>Larry</td>
                                                    <td>the Bird</td>
                                                    <td>@twitter</td>
                                                </tr>
                                                <tr>
                                                    <td>Medellín</td>
                                                    <td>Jacob</td>
                                                    <td>Thornton</td>
                                                    <td>@fat</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 pt-4 pb-5">
                        <div class="row h-100">
                            <div class="col-md-12 h-60">
                                <div class="card h-100 sc-card">
                                    <div class="card-header bg-info text-white">
                                        <i class="fas fa-history"></i> Datos Recientes
                                    </div>
                                    <div class="card-body">
                                        <table class="table text-center">
                                            <thead>
                                                <tr class="bg-info text-white">
                                                    <th scope="col">Paciente</th>
                                                    <th scope="col">En Casa</th>
                                                    <th scope="col">Ingreso</th>
                                                    <th scope="col">Ciudad</th>
                                                </tr>
                                            </thead>
                                            <tbody>
                                                <tr>
                                                    <td>Bogotá</td>
                                                    <td>Mark</td>
                                                    <td>Otto</td>
                                                    <td>@mdo</td>
                                                </tr>
                                                <tr>
                                                    <td>Medellín</td>
                                                    <td>Jacob</td>
                                                    <td>Thornton</td>
                                                    <td>@fat</td>
                                                </tr>
                                                <tr>
                                                    <td>Huila</td>
                                                    <td>Larry</td>
                                                    <td>the Bird</td>
                                                    <td>@twitter</td>
                                                </tr>
                                                <tr>
                                                    <td>Medellín</td>
                                                    <td>Jacob</td>
                                                    <td>Thornton</td>
                                                    <td>@fat</td>
                                                </tr>
                                                <tr>
                                                    <td>Bogotá</td>
                                                    <td>Mark</td>
                                                    <td>Otto</td>
                                                    <td>@mdo</td>
                                                </tr>
                                                <tr>
                                                    <td>Medellín</td>
                                                    <td>Jacob</td>
                                                    <td>Thornton</td>
                                                    <td>@fat</td>
                                                </tr>
                                                <tr>
                                                    <td>Huila</td>
                                                    <td>Larry</td>
                                                    <td>the Bird</td>
                                                    <td>@twitter</td>
                                                </tr>
                                                <tr>
                                                    <td>Medellín</td>
                                                    <td>Jacob</td>
                                                    <td>Thornton</td>
                                                    <td>@fat</td>
                                                </tr>
                                                <tr>
                                                    <td>Medellín</td>
                                                    <td>Jacob</td>
                                                    <td>Thornton</td>
                                                    <td>@fat</td>
                                                </tr>
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 h-40">
                                <div class="card h-100 mt-4 sc-card">
                                    <div class="card-header bg-warning text-white">
                                        <i class="fas fa-head-side-virus"></i> Últimos Casos
                                    </div>
                                    <div class="card-body">
                                        <h5 class="card-title">Special title</h5>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-6 h-40">
                                <div class="card h-100 mt-4 sc-card">
                                    <div class="card-header text-white" style="background: black;">
                                        <i class="fas fa-skull"></i> Últimos Fallecidos
                                    </div>
                                    <div class="card-body">
                                        <h5 class="card-title">Special title</h5>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-4 pt-4 pb-5">
                        <div class="row h-100">
                            <div class="col-md-12 h-50">
                                <div class="card h-100 sc-card">
                                    <div class="card-header bg-success text-white">
                                        <i class="fas fa-head-side-mask"></i> Curados Hoy
                                    </div>
                                    <div class="card-body"
                                        style="display: flex; justify-content: center; align-items: center;">
                                        <canvas id="myChart3" height="250" class="c-chart"></canvas>
                                        <script>
                                            var ctx = document.getElementById("myChart3").getContext("2d");
                                            var mychart = new Chart(ctx, {
                                                type: "polarArea",
                                                data: {
                                                    labels: ['Medellín', 'Bogotá', 'Huila'],
                                                    datasets: [{
                                                        label: 'My First dataset',
                                                        borderColor: [
                                                            'rgb(196, 229, 56)',
                                                            'rgb(163, 203, 56)',
                                                            'rgb(0, 148, 50)'
                                                        ],
                                                        data: [20, 10, 5],
                                                        backgroundColor: [
                                                            'rgb(196, 229, 56, 0.5)',
                                                            'rgb(163, 203, 56, 0.5)',
                                                            'rgb(0, 148, 50, 0.5)'
                                                        ]
                                                    }]
                                                }
                                            });
                                        </script>
                                    </div>

                                </div>
                            </div>
                            <div class="col-md-12 h-50">
                                <div class="card h-100 mt-4 sc-card">
                                    <div class="card-header text-white" style="background: rgb(27, 20, 100);">
                                        <i class="fas fa-book-dead"></i> Fallecidos
                                    </div>
                                    <div class="card-body">
                                        <canvas id="myChart4" class="c-chart" height="250"></canvas>
                                        <script>
                                            var ctx = document.getElementById("myChart4").getContext("2d");
                                            var mychart = new Chart(ctx, {
                                                type: "bar",
                                                data: {
                                                    labels: ['January', 'February', 'March', 'March'],
                                                    datasets: [{
                                                        label: 'My First dataset',
                                                        data: [1, 10, 5, 12],
                                                        backgroundColor: [
                                                            "rgb(27, 20, 100)",
                                                            "rgb(0, 0, 0)",
                                                            "rgb(86, 83, 82)",
                                                            "rgb(27, 20, 100)"
                                                        ],
                                                        borderColor: 'rgb(27, 20, 100)'
                                                    }]
                                                }
                                            });
                                        </script>
                                    </div>

                                </div>
                            </div>
                        </div>
                    </div>
                </div>

            </div>
            <!-- Main Content -->
        </div>

        <!-- The Modals -->
        <div class="modal fade" id="myModal">
            <div class="modal-dialog">
                <div class="modal-content c-modal">

                    <!-- Modal Header -->
                    <div class="modal-header bg-primary text-white">
                        <h4 class="modal-title">Registrar Pacientes</h4>
                        <button type="button" class="close text-white" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="example-number-input" class="col-2 col-form-label">Cédula</label>
                            <div class="col-10">
                                <input class="form-control" type="number" id="id">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-number-input" class="col-2 col-form-label">Teléfono</label>
                            <div class="col-10">
                                <input class="form-control" type="number" id="phone">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Nombre</label>
                            <div class="col-10">
                                <input class="form-control" type="text" id="name">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Apellido</label>
                            <div class="col-10">
                                <input class="form-control" type="text" id="lastname">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-email-input" class="col-2 col-form-label">Correo Electronico</label>
                            <div class="col-10">
                                <input class="form-control" type="email" id="email">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-number-input" class="col-2 col-form-label">Edad</label>
                            <div class="col-10">
                                <input class="form-control" type="number" id="age">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Dirección</label>
                            <div class="col-10">
                                <input class="form-control" type="text" id="dir">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-number-input" class="col-2 col-form-label">Estado</label>
                            <div class="col-10">
                                <select class="custom-select mb-2 mr-sm-2 mb-sm-0" name="state">
                                    <option value="selected" selected>Selecciona...</option>
                                    <option value="infect">Infectado</option>
                                    <option value="p-infect">Posible Infectado</option>
                                    <option value="n-infect">No Infectado</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-date-input" class="col-2 col-form-label">Ingreso</label>
                            <div class="col-10">
                                <input class="form-control" type="date" id="into_date">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-number-input" class="col-2 col-form-label">Casa</label>
                            <div class="col-10">
                                <select class="custom-select mb-2 mr-sm-2 mb-sm-0" name="house">
                                    <option value="selected" selected>Selecciona...</option>
                                    <option value="true">Si</option>
                                    <option value="false">No</option>
                                </select>
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Ciudad</label>
                            <div class="col-10">
                                <input class="form-control" type="text" id="city">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Localidad</label>
                            <div class="col-10">
                                <input class="form-control" type="text" id="locality">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Placa del vehículo</label>
                            <div class="col-10">
                                <input class="form-control" type="text" id="plate">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Referencia del Vehículo</label>
                            <div class="col-10">
                                <input class="form-control" type="text" id="reference">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-text-input" class="col-2 col-form-label">Nombre del Hospital</label>
                            <div class="col-10">
                                <input class="form-control" type="text" id="name_h">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-number-input" class="col-2 col-form-label">Nivel de Prioridad</label>
                            <div class="col-10">
                                <input class="form-control" type="number" id="level_h">
                            </div>
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button id="btn_register" type="button" class="btn btn-success">Registrar</button>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade" id="updateModal">
            <div class="modal-dialog">
                <div class="modal-content c-modal">

                    <!-- Modal Header -->
                    <div class="modal-header bg-primary text-white">
                        <h4 class="modal-title">Actualizar Paciente</h4>
                        <button type="button" class="close text-white" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="example-number-input" class="col-2 col-form-label">Cédula</label>
                            <div class="col-10">
                                <input class="form-control" type="number" id="example-number-input">
                            </div>
                        </div>
                        <div class="is-data">
                            <div class="form-group row">
                                <label for="example-number-input" class="col-2 col-form-label">Teléfono</label>
                                <div class="col-10">
                                    <input class="form-control" type="number" id="example-number-input">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Nombre</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" id="example-text-input">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Apellido</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" id="example-text-input">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-email-input" class="col-2 col-form-label">Correo Electronico</label>
                                <div class="col-10">
                                    <input class="form-control" type="email" id="example-email-input">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-number-input" class="col-2 col-form-label">Edad</label>
                                <div class="col-10">
                                    <input class="form-control" type="number" id="example-number-input">
                                </div>
                            </div>
                            <div class="form-group row">
                                <label for="example-text-input" class="col-2 col-form-label">Dirección</label>
                                <div class="col-10">
                                    <input class="form-control" type="text" id="example-text-input">
                                </div>
                            </div>
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary" data-dismiss="modal">Buscar</button>
                        <button type="button" class="btn btn-warning text-white is-btn"
                            data-dismiss="modal">Actualizar</button>
                    </div>

                </div>
            </div>
        </div>
        <div class="modal fade" id="searchModal">
            <div class="modal-dialog">
                <div class="modal-content c-modal">

                    <!-- Modal Header -->
                    <div class="modal-header bg-primary text-white">
                        <h4 class="modal-title">Ver Pacientes</h4>
                        <button type="button" class="close text-white" data-dismiss="modal">&times;</button>
                    </div>

                    <!-- Modal body -->
                    <div class="modal-body">
                        <div class="form-group row">
                            <label for="example-date-input" class="col-2 col-form-label">Desde</label>
                            <div class="col-10">
                                <input class="form-control" type="date" id="example-date-input">
                            </div>
                        </div>
                        <div class="form-group row">
                            <label for="example-date-input" class="col-2 col-form-label">Hasta</label>
                            <div class="col-10">
                                <input class="form-control" type="date" id="example-date-input">
                            </div>
                        </div>
                    </div>

                    <!-- Modal footer -->
                    <div class="modal-footer">
                        <button type="button" class="btn btn-primary">Buscar</button>
                    </div>

                </div>
            </div>
        </div>
        <!-- The Modals -->
        <script data-main="js/main" src="js/lib/requirejs/require.js"></script>
</body>

</html>