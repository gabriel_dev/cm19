define([], function () {
  return {
    menu_toggle: function () {
      $("#menu-toggle").click(function (e) {
        e.preventDefault();
        $("#wrapper").toggleClass("toggled");
      });
    },
  };
});