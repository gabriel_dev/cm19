require.config({
  paths: {
    dom: "dom",
    api: "../controller/api",
  },
});

require(["api", "dom"], function (api, mt) {
  mt.menu_toggle();
  $("#btn_register").click(function () {
    api.set_patient(get_data_register());
  });
  setTimeout(function () {
    api.init();
  }, 100);
});

function get_data_register() {
  let id = $("#id").val();
  let phone = $("#phone").val();
  let name = $("#name").val();
  let lastname = $("#lastname").val();
  let email = $("#email").val();
  let age = $("#age").val();
  let dir = $("#dir").val();
  let state = $("select[name=state]").val();
  let into_date = $("#into_date").val();
  let house = $("select[name=house]").val();
  let city = $("#city").val();
  let locality = $("#locality").val();
  let plate = $("#plate").val();
  let reference = $("#reference").val();
  let name_h = $("#name_h").val();
  let level_h = $("#level_h").val();

  let _data = JSON.stringify({
    id: id,
    phone: phone,
    name: name,
    lastname: lastname,
    email: email,
    age: age,
    dir: dir,
    state: state,
    into_date: into_date,
    house: house,
    city: city,
    locality: locality,
    plate: plate,
    reference: reference,
    name_h: name_h,
    level_h: level_h,
  });

  return _data;
}
