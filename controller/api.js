define([], function () {
  return {
    init: function () {
      console.log("Iniciando");
      //get data charts
    },
    set_patient:function (patient) {
      console.log("Registrando Paciente...");
      $.ajax({
        statusCode: {
          404: function () {
            console.log("Esta página no existe");
          },
        },
        url: "controller/server.php",
        method: "POST",
        data: {
          rq: "1",
          data: patient
        },
      }).done(function (data) {
        console.log("Paciente Registrado");
        console.log(data);
      });
    }
  };
});
