<?php

class Config
{

    const SAVE_CITY = "INSERT INTO ciudad(id_ciudad,nombre,localidad) VALUES(?,?,?)";

    const CURED = "SELECT COUNT(id_paciente) AS total FROM paciente WHERE estado = 'curado'";

    const GENERAL = "SELECT COUNT(id_paciente) FROM paciente WHERE estado = :cured UNION SELECT COUNT(id_paciente) FROM paciente WHERE estado = :infect UNION SELECT COUNT(id_paciente) FROM paciente WHERE estado = :dead";

    const CURRENT_CURED = "SELECT COUNT(id_paciente) FROM paciente WHERE ingreso > :current_date";

    const DEAD = "SELECT COUNT(id_paciente) FROM paciente WHERE estado = :dead";

    const CURRENT_NEW = "SELECT COUNT(id_paciente) FROM paciente WHERE ingreso = :current_date";
}
